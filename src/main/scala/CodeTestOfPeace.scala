import scala.collection.mutable

/**
 * Created by szymonwartak on 10/06/2015.
 */
object CodeTestOfPeace extends App {
  val A = List(List(1,2,3), List(2,3), List(1,44,3), List(100,6,33), List(7))
  val B = List(List(44,1), List(100,33), List(2,3), List(7,28))
  // true, true, false, false, false
//  val A = List(List(0 ,-1))
//  val B = List(List(-1), List(-1, 0))

  def one(A: List[List[Int]], B: List[List[Int]]) =
    A.map(a => B.exists(b => a.containsSlice(b)))


  def two(A: List[List[Int]], B: List[List[Int]]) = {
    // sequences of A can be computed independently (== parallel computation, spark or whatever)
    // note: could use tree from C here, but replication of tree to compute nodes might not justify matching speedup
    A.par.map(a => B.exists(b => a.containsSlice(b)))
  }


  def three(A: List[List[Int]], B: List[List[Int]]) = {
    // Use pre-built MapTree of B and check if any subsequences of A are contained.
    // With MapTree, the contains op on the huge B sequence is done in constant time for subsequences starting
    // at each index of a given sequence in A.
    // note: iterating through A could be paralellized where memory is shared because replicating the MapTree
    // will be expensive
    val b = new MapTree(B)
//    b.tree.print()
    A.map { a =>
      (1 to a.size).map(a2 => a.drop(a2 - 1)).exists{
        aSub => b.contains(aSub, true)
      }
    }

    // alternative: (in case MapTree doesn't fit in memory - e.g., B is gigantic and elements of B lists vary widely)
    // constructing a Bloom filter on massive B can help us determine falses quickly for elements of A and save us
    // doing the massive search in many cases
  }


  /**
   * Tree structure designed for compact storage and efficient (constant time) contains op
   */
  class MapTree(initialItems: List[List[Int]]) {
    class MapTreeNode {
      var isLeaf = false
      def setLeaf(isLeaf: Boolean) { this.isLeaf = isLeaf }
      lazy val children = mutable.HashMap[Int, MapTreeNode]()
      def print(indent: String = ""): Unit = {
        children.foreach{ x =>
          println(s"$indent${x._1} - ${x._2.isLeaf}")
          x._2.print(s"$indent\t")
        }
      }
    }
    val tree = new MapTreeNode()
    def add(seq: List[Int]): Unit = {
      val leaf = seq.foldLeft(tree){ (node, x) =>
        node.children.getOrElseUpdate(x, new MapTreeNode())
      }
      leaf.setLeaf(true)
    }
    // acceptSubsequenceMatches: matching seq < 1, 2, 3 > in tree containing < 1, 2 > will yield true
    def contains(seq: List[Int], acceptSubsequenceMatches: Boolean = false): Boolean = {
      var currentNode = tree
      seq.foreach { x =>
        currentNode.children.get(x) match {
          case Some(node) if acceptSubsequenceMatches && node.isLeaf => return true
          case None => return false
          case _ => currentNode = currentNode.children(x)
        }
      }
      currentNode.isLeaf // end on a leaf
    }
    initialItems foreach add
  }

}
