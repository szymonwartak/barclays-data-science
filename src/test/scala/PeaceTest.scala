import org.scalacheck.Properties
import org.scalacheck.Prop.forAll

object SubsequenceSpecification extends Properties("Subsequence matching") {

  import CodeTestOfPeace._
  property("c") = forAll { (A: List[List[Int]], B: List[List[Int]]) =>
    if (A.size > 0 && B.size > 0 && A.forall(_.size > 0) && B.forall(_.size > 0))
      one(A, B) == three(A, B)
    else true
  }

}